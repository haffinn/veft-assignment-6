﻿using System.Collections.Generic;
using System.Linq;
using CoursesAPI.Models;
using CoursesAPI.Services.DataAccess;
using CoursesAPI.Services.Exceptions;
using CoursesAPI.Services.Models.Entities;

namespace CoursesAPI.Services.Services
{
	public class CoursesServiceProvider
	{
		private readonly IUnitOfWork _uow;

		private readonly IRepository<CourseInstance> _courseInstances;
		private readonly IRepository<TeacherRegistration> _teacherRegistrations;
		private readonly IRepository<CourseTemplate> _courseTemplates; 
		private readonly IRepository<Person> _persons;

		public CoursesServiceProvider(IUnitOfWork uow)
		{
			_uow = uow;

			_courseInstances      = _uow.GetRepository<CourseInstance>();
			_courseTemplates      = _uow.GetRepository<CourseTemplate>();
			_teacherRegistrations = _uow.GetRepository<TeacherRegistration>();
			_persons              = _uow.GetRepository<Person>();
		}


		/// <summary>
		/// You should write tests for this function. You will also need to
		/// modify it, such that it will correctly return the name of the main
		/// teacher of each course.
		/// </summary>
		/// <param name="semester"></param>
		/// <returns></returns>
		public List<CourseInstanceDTO> GetCourseInstancesBySemester(string semester = null)
		{
			if (string.IsNullOrEmpty(semester))
			{
				semester = "20153";
			}

			var courses = (from c in _courseInstances.All()
				join ct in _courseTemplates.All() on c.CourseID equals ct.CourseID
				where c.SemesterID == semester
				select new CourseInstanceDTO
				{
					Name               = ct.Name,
					TemplateID         = ct.CourseID,
					CourseInstanceID   = c.ID,
					MainTeacher        = ""
				}).ToList();

			return courses;
		}

        /// <summary>
        /// Get a course instance by ID
        /// </summary>
        /// <param name="id">ID of the course</param>
        /// <returns>A detailed info on the course</returns>
        public CourseInstanceDTO GetCourseByID(int id)
        {
            var course = (from c in _courseInstances.All()
                          join ct in _courseTemplates.All() on c.CourseID equals ct.CourseID
                          where c.ID == id
                          select new CourseInstanceDTO
                          {
                              Name = ct.Name,
                              TemplateID = ct.CourseID,
                              CourseInstanceID = c.ID,
                              MainTeacher = ""
                          }).SingleOrDefault();

            if (course == null)
            {
                throw new AppObjectNotFoundException();
            }

            return course;
        }
	}
}
