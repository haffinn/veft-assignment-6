﻿using System.Net;
using System.Web.Http;
using CoursesAPI.Models;
using CoursesAPI.Services.DataAccess;
using CoursesAPI.Services.Exceptions;
using CoursesAPI.Services.Services;
using WebApi.OutputCache.V2;

namespace CoursesAPI.Controllers
{
    [Authorize]
	[RoutePrefix("api/courses")]
	public class CoursesController : ApiController
	{
		private readonly CoursesServiceProvider _service;

		public CoursesController()
		{
			_service = new CoursesServiceProvider(new UnitOfWork<AppDataContext>());
		}

		[HttpGet]
		[AllowAnonymous]
        [CacheOutput(ClientTimeSpan = 86400, ServerTimeSpan = 86400)]
        [Route("")]
		public IHttpActionResult GetCoursesBySemester(string semester = null)
		{
			return Ok(_service.GetCourseInstancesBySemester(semester));
		}

        /// <summary>
        /// Get a specific course instance by ID
        /// </summary>
        /// <param name="id">The ID of the course</param>
        /// <returns>A course instance</returns
        [CacheOutput(ClientTimeSpan = 86400, ServerTimeSpan = 86400)]
        public IHttpActionResult GetCourseInstancesByID(int id)
        {
            try
            {
                return Ok(_service.GetCourseByID(id));
            }
            catch (AppObjectNotFoundException e)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }
        }


        /// <summary>
        /// Add a new course.
        /// This does nothing but return correct status code.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [InvalidateCacheOutput("GetCourseInstancesBySemester")]
        public IHttpActionResult AddNewCourse()
        {
            return StatusCode(HttpStatusCode.Created);
        }
	}
}
